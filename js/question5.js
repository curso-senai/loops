function viewRepet() {
    let inpNome = nome.value;
    let inpNota1 = Number(nota1.value);
    let inpNota2 = Number(nota2.value);
    let inpNota3 = Number(nota3.value);
    let inpNota4 = Number(nota4.value);
    let result = "";

    let media = (inpNota1 + inpNota2 + inpNota3 + inpNota4) / 4;
    if(media < 60){
        result = 'F';
    } else if(media < 70){
        result = 'D';
    }else if(media < 80){
        result = 'C';
    }else if(media < 90){
        result = 'B';
    }else if(media <= 100){
        result = 'A';
    }else {
        result = "Media invalida";
    }

    return `Aluno ${inpNome}, nota ${result}`;
}

$(document).ready(function () {
    $("#btn_print").click(function () {
        $("#resp").html(viewRepet);
        $("#resp-hidden").slideDown("slow");
    });
});
