function viewRepet(){
    let inpCoelhos = Number(coelhos.value);
    let inpAnos = Number(anos.value);

    for (let i = 0; i < inpAnos; i++){
        if(i == 1) continue;
        inpCoelhos *= 7;
    }

    return inpCoelhos;
}

$(document).ready(function(){
    $("#btn_print").click(function(){
        $("#resp").html(viewRepet);
        $("#resp-hidden").slideDown("slow");
    });
});
