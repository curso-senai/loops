function printNumbers(){
    let result = "";
    for (let i = 0; i <= 20; i++){
        result += `${i} <br>`;
    }
    resp.innerHTML = result;
}

btn_print.addEventListener('click', printNumbers);

$(document).ready(function(){
    $("#btn_print").click(function(){
        $("#resp-hidden").slideDown("slow");
    });
});
